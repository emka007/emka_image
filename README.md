# ..::README::..

## **Image Resizer be eMKa**

Simple application to downsize any image (\*.jpg, \*.jpeg, \*.png) or crop it with drawn rectangle.
Aplikacja s�u��ca do zminiejszania rozmiaru dowolnego obrazu (\*.jpg, \*.jpeg,\*.png) lub przyci�cia zgodnie z zaznaczonym obszarem.

- *v1.1*  Added Crop Function. 
Now image is display in piceture box with its own aspect ratio. 
Minor changes.
- *v1.0*  Added Resize Function
Downsize code from: stackoverflow.com/questions/1922040/resize-an-image-c-sharp 
In thanks to: mpen user. Thank You.

* * *
Repo owner: Michal Krzysztynski;   
			michal.krzysztynski@gmail.com;   
			Home site:  [http://student.agh.edu.pl/~krzyszty](http://student.agh.edu.pl/~krzyszty)  
