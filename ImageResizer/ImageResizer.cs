﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ImageResizer
{
    public partial class ImageResizer : Form
    {
        public ImageResizer()
        {
            InitializeComponent();
        }

        string ImgPath = null;
        Image image1;
        Point startPos;      // mouse-down position
        Point currentPos;    // current mouse position
        bool drawing;        // busy drawing
        List<Rectangle> rectangles = new List<Rectangle>();  // previous rectangles
        int VievType;
        float asp;

        float type1_heightCoef;
        float type2_widthCoef;

        

        private void WartoscX_ValueChanged(object sender, EventArgs e)
        {
            if (image1 == null)
            {
                MessageBox.Show("Please select image first :).");
                WartoscX.Value = 0;
            }

            if (Hold_Aspect.Checked)
            {
                if ((int)(WartoscY.Value = WartoscX.Value * image1.Height / image1.Width) < 1)
                {
                    MessageBox.Show("Width and Height must be > 0 values. :)");
                }
            }
        }

        private void WartoscY_ValueChanged(object sender, EventArgs e)
        {
            if (image1 == null)
            {
                MessageBox.Show("Please select image first :).");
                WartoscY.Value = 0;
            }

            if (checkBox1.Checked)
            {
                if ((int)(WartoscX.Value = WartoscY.Value * image1.Width / image1.Height) < 1)
                {
                    MessageBox.Show("Width and Height must be > 0 values. :)");
                }
            }
        }

        private void Hold_Aspect_CheckedChanged(object sender, EventArgs e)
        {
            if (Hold_Aspect.Checked)
            {
                WartoscY.Enabled = false;
                checkBox1.Checked = false;
            }
            else
            {
                WartoscY.Enabled = true;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                WartoscX.Enabled = false;
                Hold_Aspect.Checked = false;
            }
            else
            {
                WartoscX.Enabled = true;
            }

        }

        private void MenuOpenImage_Click(object sender, EventArgs e)
        {
            if (rectangles.Count > 0)
                rectangles.RemoveAt(0);

            Stream myStream = null;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            string path = Directory.GetCurrentDirectory();

            openFileDialog1.InitialDirectory = path;
            openFileDialog1.Filter = "jpg files (*.jpg)|*.jpg|jpeg files (*.jpeg)|*.jpeg|png files (*.png)|*.png|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            image1 = Image.FromFile(openFileDialog1.FileName);
                            ImgPath = openFileDialog1.FileName;
                            openFileDialog1.RestoreDirectory = true;

                            WartoscX.Value = WartoscX.Maximum = image1.Width;
                            WartoscY.Value = WartoscY.Maximum = image1.Height;

                            name.Visible = true;
                            name.Text = openFileDialog1.SafeFileName;
                            height.Visible = true;
                            height.Text = image1.Height.ToString();
                            width.Visible = true;
                            width.Text = image1.Width.ToString();
                            dest.Visible = true;
                            dest.Text = openFileDialog1.FileName;

                            aspect.Visible = true;
                            asp = image1.Width / (float)image1.Height;

                            if (image1.Width == image1.Height)
                            {
                                aspect.Text = "1:1";
                            }
                            else if (asp >= 1.77 && asp <= 1.79)
                            {
                                aspect.Text = "16:9";
                            }
                            else if (asp >= 1.32 && asp <= 1.34)
                            {
                                aspect.Text = "4:3";
                            }
                            else
                            {
                                aspect.Text = asp.ToString("F2");
                            }

                            Podglad1.Image = new Bitmap(Podglad1.Width, Podglad1.Height);
                            using (Graphics g = Graphics.FromImage(Podglad1.Image))
                            {
                                g.Clear(Color.FromArgb(216, 216, 216));

                                if (image1.Width > image1.Height)
                                {
                                    if (image1.Width > Podglad1.Width)
                                    {
                                        type1_heightCoef = 1 / asp * Podglad1.Width;
                                        g.DrawImage(image1, 0, 0, Podglad1.Width, type1_heightCoef);
                                        VievType = 1;       //skalowanie wg szerokości. szerokośc 100%.
                                    }
                                    else                    //mniejsze 
                                    {
                                        g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
                                        VievType = 3;
                                    }
                                }
                                else
                                {
                                    if (image1.Height > Podglad1.Height)        //wysokosc to 100%
                                    {
                                        type2_widthCoef = asp * Podglad1.Height;
                                        g.DrawImage(image1, 0, 0, type2_widthCoef, Podglad1.Height);
                                        VievType = 2;
                                    }
                                    else
                                    {
                                        g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
                                        VievType = 3;
                                    }
                                }
                            }

                            string dir = Path.GetDirectoryName(ImgPath);
                            string fileName = Path.GetFileNameWithoutExtension(ImgPath);
                            string fileExt = Path.GetExtension(ImgPath);

                            destination.Text = Path.Combine(dir, fileName + "_resized" + fileExt);

                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                    return;
                }
            }
        }

        private void SaveDest_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:\";
            saveFileDialog1.CheckPathExists = true;
            saveFileDialog1.DefaultExt = "jpg";
            saveFileDialog1.Filter = "jpg files (*.jpg)|*.jpg|jpeg files (*.jpeg)|*.jpeg|png files (*.png)|*.png";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                destination.Text = saveFileDialog1.FileName;
            }
        }

        private void RESIZE_Click(object sender, EventArgs e)
        {
            if (ImgPath == null)
            {
                MessageBox.Show("Please, open image file first. Left-top corner ;).");
                return;
            }
            else if (WartoscX.Value == 0 || WartoscY.Value == 0)
            {
                MessageBox.Show("Width and Height must be > 0 values. :)");
                return;
            }
            else if (destination.TextLength == 0)
            {
                MessageBox.Show("Please set destination, where to save resized piceture. :)");
                return;
            }
            else if(rectangles[0].X < 0 || rectangles[0].Y < 0)
            {
                MessageBox.Show("Crop rectangle X || Y < 0.\nTop-left corner have to be on image.");
                return;
            }
            else if (VievType == 1)          //szerokosc to 100%
            {
                if( (rectangles[0].Y * image1.Height / type1_heightCoef) > image1.Height)
                {
                    MessageBox.Show("Nothing to be cropped.");
                    return;
                } 
            }
            else if (VievType == 2)     //wysokosc to 100%
            {
                if( (rectangles[0].X * image1.Width / type2_widthCoef) > image1.Width )
                {
                    MessageBox.Show("Nothing to be cropped.");
                    return;
                }
            }
            else if (VievType == 3)           //mniejsze.
            {
                if(rectangles[0].X > image1.Width && rectangles[0].Y > image1.Height)
                {
                    MessageBox.Show("Nothing to be cropped.");
                    return;
                }
            }
            

            if (ResizeCheckbox.Checked)
            {
                var image2 = ResizeImage(image1, (int)WartoscX.Value, (int)WartoscY.Value);
                if (File.Exists(destination.Text))
                {
                    File.Delete(destination.Text);
                    image2.Save(destination.Text, ImageFormat.Jpeg);
                }
                else image2.Save(destination.Text, ImageFormat.Jpeg);

                MessageBox.Show(String.Format("Image resized and exported to:\n{0}", destination.Text));
            }
            else if (CropCheckbox.Checked)
            {

                int CropStartX;
                int CropStartY;
                int cropX;
                int cropY;
                
                if (VievType == 1)          //szerokosc to 100%
                {
                    CropStartX = (int)(rectangles[0].X / (float)Podglad1.Width * image1.Width);
                    CropStartY = (int)(rectangles[0].Y * image1.Height / type1_heightCoef);
                    cropX = (int)(rectangles[0].Width / (float)Podglad1.Width * image1.Width);
                    cropY = (int)(rectangles[0].Height * image1.Height / type1_heightCoef);
                }
                else if (VievType == 2)     //wysokosc to 100%
                {
                    CropStartX = (int)(rectangles[0].X * image1.Width / type2_widthCoef);
                    CropStartY = (int)(rectangles[0].Y / (float)Podglad1.Height * image1.Height);
                    cropX = (int)(rectangles[0].Width * image1.Width / type2_widthCoef);
                    cropY = (int)(rectangles[0].Height / (float)Podglad1.Height * image1.Height);
                }
                else                        //mniejsze.
                {
                    CropStartX = rectangles[0].X;
                    CropStartY = rectangles[0].Y;
                    cropX = rectangles[0].Width;
                    cropY = rectangles[0].Height;
                }
                
                Rectangle R1 = new Rectangle(CropStartX, CropStartY, cropX, cropY);

                var image2 = cropImage(image1, R1);

                if (File.Exists(destination.Text))
                {
                    File.Delete(destination.Text);
                    image2.Save(destination.Text, ImageFormat.Jpeg);
                }
                else image2.Save(destination.Text, ImageFormat.Jpeg);

                MessageBox.Show(String.Format("Image cropped and exported to:\n{0}", destination.Text));
            }
        }

        //Cropp code from: stackoverflow.com/questions/734930/how-to-crop-an-image-using-c#734938       Thank You Bern and Nick. :)
        private static Image cropImage(Image img, Rectangle cropArea)
        {
            Bitmap bmpImage = new Bitmap(img);
            return bmpImage.Clone(cropArea, bmpImage.PixelFormat);
        }

        //Resize code from: stackoverflow.com/questions/1922040/resize-an-image-c-sharp                 Thank You, mpen :)
        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        private void Crop_CheckChange(object sender, EventArgs e)
        {
            if (CropCheckbox.Checked)
            {
                Podglad1.Cursor = Cursors.Cross;
                ResizeCheckbox.Checked = false;
                WartoscX.Enabled = false;
                WartoscY.Enabled = false;
                Hold_Aspect.Enabled = false;
                checkBox1.Enabled = false;

                if (ImgPath != null)
                {
                    string dir = Path.GetDirectoryName(ImgPath);
                    string fileName = Path.GetFileNameWithoutExtension(ImgPath);
                    string fileExt = Path.GetExtension(ImgPath);

                    destination.Text = Path.Combine(dir, fileName + "_cropped" + fileExt);
                }

                RESIZE.Text = "CROP ME :)";
            }
            else
                ResizeCheckbox.Checked = true;
        }

        private void Resize_CheckChange(object sender, EventArgs e)
        {
            if (ResizeCheckbox.Checked)
            {
                if (rectangles.Count > 0)
                {
                    rectangles.RemoveAt(0);
                    Podglad1.Invalidate();
                }

                Podglad1.Cursor = Cursors.Default;
                CropCheckbox.Checked = false;
                WartoscX.Enabled = true;
                WartoscY.Enabled = true;
                Hold_Aspect.Enabled = true;
                checkBox1.Enabled = true;
                RectangleCoordLabel.Visible = false;

                if (ImgPath != null)
                {
                    string dir = Path.GetDirectoryName(ImgPath);
                    string fileName = Path.GetFileNameWithoutExtension(ImgPath);
                    string fileExt = Path.GetExtension(ImgPath);

                    destination.Text = Path.Combine(dir, fileName + "_resized" + fileExt);
                }

                RESIZE.Text = "RESIZE ME :)";
            }
            else
                CropCheckbox.Checked = true;
        }

        private Rectangle getRectangle()
        {
            return new Rectangle(
                Math.Min(startPos.X, currentPos.X),
                Math.Min(startPos.Y, currentPos.Y),
                Math.Abs(startPos.X - currentPos.X),
                Math.Abs(startPos.Y - currentPos.Y));
        }

        private void Podglad1_MouseDown(object sender, MouseEventArgs e)
        {
            if (ResizeCheckbox.Checked)
            {
                return;
            }

            currentPos = startPos = e.Location;
            drawing = true;
        }

        private void Podglad1_MouseMove(object sender, MouseEventArgs e)
        {
            if (ImgPath == null)
            {
                return;
            }

            if (CropCheckbox.Checked)
            {
                currentPos = e.Location;
                if (drawing) Podglad1.Invalidate();
            }

            int X;
            int Y;
            
            if (VievType == 1)          //szerokosc to 100%
            {
                X = (int)(e.X / (float)Podglad1.Width * image1.Width);
                Y = (int)(e.Y * image1.Height / type1_heightCoef);
            }
            else if (VievType == 2)     //wysokosc to 100%
            {
                X = (int)(e.X * image1.Width / type2_widthCoef);
                Y = (int)(e.Y / (float)Podglad1.Height * image1.Height);
            }
            else                        //mniejsze.
            {
                X = e.X;
                Y = e.Y;
            }
            
            ImageMouseCoord.Text = String.Format("(X : Y)   {0} : {1} [px]", X, Y);

            if (X > image1.Width || Y > image1.Height)
                ImageMouseCoord.ForeColor = Color.Red;
            else
                ImageMouseCoord.ForeColor = Color.Green;

        }

        private void Podglad1_MouseUp(object sender, MouseEventArgs e)
        {
            if (drawing)
            {
                drawing = false;
                var rc = getRectangle();
                if (rc.Width > 0 && rc.Height > 0)
                {
                    if (rectangles.Count > 0)
                        rectangles.RemoveAt(0);
                    rectangles.Add(rc);
                }
                Podglad1.Invalidate();

                if (ImgPath != null)
                {
                    RectangleCoordLabel.Visible = true;

                    if (VievType == 1)          //szerokosc to 100%
                    {
                        RectangleCoordLabel.Text = String.Format("Rectangle to crop size (X : Y)   {0} : {1} [px]",
                                                    (int)( rectangles[0].Width / (float)Podglad1.Width * image1.Width),
                                                    (int)( rectangles[0].Height * image1.Height / type1_heightCoef) );
                    }
                    else if (VievType == 2)     //wysokosc to 100%
                    {
                        RectangleCoordLabel.Text = String.Format("Rectangle to crop size (X : Y)   {0} : {1} [px]",
                                                    (int)( rectangles[0].Width * image1.Width / type2_widthCoef),
                                                    (int)( rectangles[0].Height / (float)Podglad1.Height * image1.Height));
                    }
                    else                        //mniejsze.
                    {
                        RectangleCoordLabel.Text = String.Format("Rectangle to crop size (X : Y)   {0} : {1} [px]",
                                                    rectangles[0].Width,
                                                    rectangles[0].Height);
                    }
                    
                }
            }
        }

        private void Podglad1_Paint(object sender, PaintEventArgs e)
        {
            if (rectangles.Count > 0) e.Graphics.DrawRectangles(Pens.Black, rectangles.ToArray());
            if (drawing) e.Graphics.DrawRectangle(Pens.Red, getRectangle());
        }

    }
}