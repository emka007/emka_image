﻿namespace ImageResizer
{
    partial class ImageResizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageResizer));
            this.Podglad1 = new System.Windows.Forms.PictureBox();
            this.RESIZE = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Hold_Aspect = new System.Windows.Forms.CheckBox();
            this.destination = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.MenuOpenImage = new System.Windows.Forms.ToolStripButton();
            this.SaveDest = new System.Windows.Forms.Button();
            this.WartoscX = new System.Windows.Forms.NumericUpDown();
            this.WartoscY = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dest = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.aspect = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.height = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.width = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.Panel_Name = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ResizeCheckbox = new System.Windows.Forms.CheckBox();
            this.CropCheckbox = new System.Windows.Forms.CheckBox();
            this.RectangleCoordLabel = new System.Windows.Forms.Label();
            this.ImageMouseCoord = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Podglad1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WartoscX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WartoscY)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Podglad1
            // 
            this.Podglad1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Podglad1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Podglad1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Podglad1.InitialImage = ((System.Drawing.Image)(resources.GetObject("Podglad1.InitialImage")));
            this.Podglad1.Location = new System.Drawing.Point(364, 40);
            this.Podglad1.Name = "Podglad1";
            this.Podglad1.Size = new System.Drawing.Size(727, 421);
            this.Podglad1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Podglad1.TabIndex = 0;
            this.Podglad1.TabStop = false;
            this.Podglad1.Paint += new System.Windows.Forms.PaintEventHandler(this.Podglad1_Paint);
            this.Podglad1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Podglad1_MouseDown);
            this.Podglad1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Podglad1_MouseMove);
            this.Podglad1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Podglad1_MouseUp);
            // 
            // RESIZE
            // 
            this.RESIZE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.RESIZE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RESIZE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RESIZE.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.RESIZE.FlatAppearance.BorderSize = 0;
            this.RESIZE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RESIZE.Font = new System.Drawing.Font("Minion Pro SmBd", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RESIZE.ForeColor = System.Drawing.Color.Black;
            this.RESIZE.Location = new System.Drawing.Point(27, 410);
            this.RESIZE.Name = "RESIZE";
            this.RESIZE.Size = new System.Drawing.Size(311, 51);
            this.RESIZE.TabIndex = 1;
            this.RESIZE.Text = "RESIZE ME :)";
            this.toolTip1.SetToolTip(this.RESIZE, "Resizze image and save it to location.");
            this.RESIZE.UseVisualStyleBackColor = false;
            this.RESIZE.Click += new System.EventHandler(this.RESIZE_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Horizontal Size | Width";
            this.toolTip1.SetToolTip(this.label1, "Specify new image width.");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Vertical Size | Height";
            this.toolTip1.SetToolTip(this.label2, "Specify new image height.");
            // 
            // Hold_Aspect
            // 
            this.Hold_Aspect.AutoSize = true;
            this.Hold_Aspect.Location = new System.Drawing.Point(27, 277);
            this.Hold_Aspect.Name = "Hold_Aspect";
            this.Hold_Aspect.Size = new System.Drawing.Size(198, 21);
            this.Hold_Aspect.TabIndex = 6;
            this.Hold_Aspect.Text = "Hold Aspect, specify Width";
            this.Hold_Aspect.UseVisualStyleBackColor = true;
            this.Hold_Aspect.CheckedChanged += new System.EventHandler(this.Hold_Aspect_CheckedChanged);
            // 
            // destination
            // 
            this.destination.Location = new System.Drawing.Point(27, 372);
            this.destination.Name = "destination";
            this.destination.Size = new System.Drawing.Size(311, 22);
            this.destination.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 343);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Where to save?";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuOpenImage});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1103, 27);
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // MenuOpenImage
            // 
            this.MenuOpenImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.MenuOpenImage.Image = ((System.Drawing.Image)(resources.GetObject("MenuOpenImage.Image")));
            this.MenuOpenImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MenuOpenImage.Name = "MenuOpenImage";
            this.MenuOpenImage.Size = new System.Drawing.Size(76, 24);
            this.MenuOpenImage.Text = "Open File";
            this.MenuOpenImage.Click += new System.EventHandler(this.MenuOpenImage_Click);
            // 
            // SaveDest
            // 
            this.SaveDest.Location = new System.Drawing.Point(138, 340);
            this.SaveDest.Name = "SaveDest";
            this.SaveDest.Size = new System.Drawing.Size(183, 23);
            this.SaveDest.TabIndex = 10;
            this.SaveDest.Text = "Save Destination";
            this.SaveDest.UseVisualStyleBackColor = true;
            this.SaveDest.Click += new System.EventHandler(this.SaveDest_Click);
            // 
            // WartoscX
            // 
            this.WartoscX.Location = new System.Drawing.Point(194, 202);
            this.WartoscX.Name = "WartoscX";
            this.WartoscX.Size = new System.Drawing.Size(120, 22);
            this.WartoscX.TabIndex = 11;
            this.WartoscX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WartoscX.ValueChanged += new System.EventHandler(this.WartoscX_ValueChanged);
            // 
            // WartoscY
            // 
            this.WartoscY.Location = new System.Drawing.Point(194, 240);
            this.WartoscY.Name = "WartoscY";
            this.WartoscY.Size = new System.Drawing.Size(120, 22);
            this.WartoscY.TabIndex = 12;
            this.WartoscY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WartoscY.ValueChanged += new System.EventHandler(this.WartoscY_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.dest);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.aspect);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.height);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.width);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.name);
            this.panel1.Controls.Add(this.Panel_Name);
            this.panel1.Location = new System.Drawing.Point(27, 67);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(311, 129);
            this.panel1.TabIndex = 13;
            // 
            // dest
            // 
            this.dest.AutoSize = true;
            this.dest.Location = new System.Drawing.Point(162, 76);
            this.dest.Name = "dest";
            this.dest.Size = new System.Drawing.Size(33, 17);
            this.dest.TabIndex = 9;
            this.dest.Text = "-----";
            this.dest.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Destination:";
            // 
            // aspect
            // 
            this.aspect.AutoSize = true;
            this.aspect.Location = new System.Drawing.Point(162, 59);
            this.aspect.Name = "aspect";
            this.aspect.Size = new System.Drawing.Size(33, 17);
            this.aspect.TabIndex = 7;
            this.aspect.Text = "-----";
            this.aspect.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(131, 17);
            this.label10.TabIndex = 6;
            this.label10.Text = "Aspect Ratio [W/H]:";
            // 
            // height
            // 
            this.height.AutoSize = true;
            this.height.Location = new System.Drawing.Point(162, 42);
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(33, 17);
            this.height.TabIndex = 5;
            this.height.Text = "-----";
            this.height.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "Height:";
            // 
            // width
            // 
            this.width.AutoSize = true;
            this.width.Location = new System.Drawing.Point(162, 25);
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(33, 17);
            this.width.TabIndex = 3;
            this.width.Text = "-----";
            this.width.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Width:";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Location = new System.Drawing.Point(162, 8);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(33, 17);
            this.name.TabIndex = 1;
            this.name.Text = "-----";
            this.name.Visible = false;
            // 
            // Panel_Name
            // 
            this.Panel_Name.AutoSize = true;
            this.Panel_Name.Location = new System.Drawing.Point(12, 8);
            this.Panel_Name.Name = "Panel_Name";
            this.Panel_Name.Size = new System.Drawing.Size(49, 17);
            this.Panel_Name.TabIndex = 0;
            this.Panel_Name.Text = "Name:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(26, 304);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(203, 21);
            this.checkBox1.TabIndex = 14;
            this.checkBox1.Text = "Hold Aspect, specify Height";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // ResizeCheckbox
            // 
            this.ResizeCheckbox.AutoSize = true;
            this.ResizeCheckbox.Checked = true;
            this.ResizeCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ResizeCheckbox.Location = new System.Drawing.Point(53, 40);
            this.ResizeCheckbox.Name = "ResizeCheckbox";
            this.ResizeCheckbox.Size = new System.Drawing.Size(79, 21);
            this.ResizeCheckbox.TabIndex = 16;
            this.ResizeCheckbox.Text = "RESIZE";
            this.ResizeCheckbox.UseVisualStyleBackColor = true;
            this.ResizeCheckbox.CheckedChanged += new System.EventHandler(this.Resize_CheckChange);
            // 
            // CropCheckbox
            // 
            this.CropCheckbox.AutoSize = true;
            this.CropCheckbox.Location = new System.Drawing.Point(194, 40);
            this.CropCheckbox.Name = "CropCheckbox";
            this.CropCheckbox.Size = new System.Drawing.Size(69, 21);
            this.CropCheckbox.TabIndex = 17;
            this.CropCheckbox.Text = "CROP";
            this.CropCheckbox.UseVisualStyleBackColor = true;
            this.CropCheckbox.CheckedChanged += new System.EventHandler(this.Crop_CheckChange);
            // 
            // RectangleCoordLabel
            // 
            this.RectangleCoordLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.RectangleCoordLabel.Location = new System.Drawing.Point(596, 461);
            this.RectangleCoordLabel.Name = "RectangleCoordLabel";
            this.RectangleCoordLabel.Size = new System.Drawing.Size(495, 23);
            this.RectangleCoordLabel.TabIndex = 18;
            this.RectangleCoordLabel.Text = "RectCoord";
            this.RectangleCoordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RectangleCoordLabel.Visible = false;
            // 
            // ImageMouseCoord
            // 
            this.ImageMouseCoord.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ImageMouseCoord.Location = new System.Drawing.Point(361, 461);
            this.ImageMouseCoord.Name = "ImageMouseCoord";
            this.ImageMouseCoord.Size = new System.Drawing.Size(229, 23);
            this.ImageMouseCoord.TabIndex = 19;
            this.ImageMouseCoord.Text = "Coordinates";
            this.ImageMouseCoord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ImageResizer
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 482);
            this.Controls.Add(this.ImageMouseCoord);
            this.Controls.Add(this.RectangleCoordLabel);
            this.Controls.Add(this.CropCheckbox);
            this.Controls.Add(this.ResizeCheckbox);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.WartoscY);
            this.Controls.Add(this.WartoscX);
            this.Controls.Add(this.SaveDest);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.destination);
            this.Controls.Add(this.Hold_Aspect);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RESIZE);
            this.Controls.Add(this.Podglad1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImageResizer";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "eMKa Image Resizer";
            ((System.ComponentModel.ISupportInitialize)(this.Podglad1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WartoscX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WartoscY)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Podglad1;
        private System.Windows.Forms.Button RESIZE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox Hold_Aspect;
        private System.Windows.Forms.TextBox destination;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton MenuOpenImage;
        private System.Windows.Forms.Button SaveDest;
        public System.Windows.Forms.NumericUpDown WartoscX;
        public System.Windows.Forms.NumericUpDown WartoscY;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Panel_Name;
        private System.Windows.Forms.Label aspect;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label height;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label width;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label dest;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox ResizeCheckbox;
        private System.Windows.Forms.CheckBox CropCheckbox;
        private System.Windows.Forms.Label RectangleCoordLabel;
        private System.Windows.Forms.Label ImageMouseCoord;
    }
}

